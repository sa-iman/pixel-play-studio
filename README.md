# Editor Code Test

This code written by Eng. Iman AlSamman for a code test for Frontend Developer Position.

## Explaination

The libraries I used:

- Vue 3

- Pinia: Shared state management

- TailwindCss: For full customization in styles

- Vue Advanced Cropper: For cropping image

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

## Notes

- Saving video is not supported
