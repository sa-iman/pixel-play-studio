import { defineStore } from 'pinia'

export const useAdjustmentStore = defineStore('adjustment',  {
  state: () => ({ 
    brightness: 100,
    contrast: 100,
    rotate: 0,
    cropping: false,
    save:false
  }),
  getters: {
  },
  actions: {
    setSave(val) {
this.save = val
    },
    setCropping(val) {
      this.cropping = val
    },
    setBrightness(val) {
      this.brightness = val;  
      },
      setContrast(val) {
        this.contrast = val;  
        },
        resetRotate() {
          this.rotate = 0;

        },
        setRotate() {
          this.rotate += 45;

          if (this.rotate >= 360) {
            this.rotate = 0;
          }          },
  },
  
})
